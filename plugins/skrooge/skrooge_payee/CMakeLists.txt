#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_PAYEE ::..")

PROJECT(plugin_payee)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skrooge_payee_SRCS
	skgpayeeplugin.cpp
	skgpayeepluginwidget.cpp)

ki18n_wrap_ui(skrooge_payee_SRCS skgpayeepluginwidget_base.ui skgpayeepluginwidget_pref.ui)
kconfig_add_kcfg_files(skrooge_payee_SRCS skgpayee_settings.kcfgc )

ADD_LIBRARY(skrooge_payee MODULE ${skrooge_payee_SRCS})
TARGET_LINK_LIBRARIES(skrooge_payee KF5::Parts KF5::ItemViews skgbasemodeler skgbasegui skgbankmodeler skgbankgui)

########### install files ###############
INSTALL(TARGETS skrooge_payee DESTINATION ${KDE_INSTALL_QTPLUGINDIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/org.kde.skrooge-plugin-payee.desktop DESTINATION ${KDE_INSTALL_KSERVICES5DIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/skrooge_payee.rc  DESTINATION  ${KDE_INSTALL_KXMLGUI5DIR}/skrooge_payee )
INSTALL(FILES ${PROJECT_SOURCE_DIR}/skgpayee_settings.kcfg  DESTINATION  ${KDE_INSTALL_KCFGDIR} )

