<!--
/***************************************************************************
 * SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
 * SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
 * SPDX-License-Identifier: GPL-3.0-or-later
 ***************************************************************************/
-->
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <style>
    body {padding-top: 70px; padding-bottom: 70px; }
    td {white-space: nowrap;}
    @media print {a[href]:after {content: "" !important;}} 
    meter {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
  
        width: 200px;
        height: 30px;
    }
    meter::-webkit-meter-bar {
        background: #EEE;
        box-shadow: 0 2px 3px rgba(0,0,0,0.2) inset;
        border-radius: 3px;
    }

    meter::-webkit-meter-optimum-value {
        background: #86CC00; /* Green */
        border-radius: 3px;
    }

    meter::-webkit-meter-suboptimum-value {
        background: #FFDB1A; /* Yellow */
        border-radius: 3px;
    }

    meter::-webkit-meter-even-less-good-value {
        background: #CC4600; /* Red */
        border-radius: 3px;
    }    
    </style>
  </head>
  <body>
    <script src="https://code.jquery.com/jquery.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
        <h1>{{ title_main }}</h1>
        </div>
      </div>
    </div>  

    <div class="navbar navbar-default navbar-fixed-bottom" role="navigation">
      <div class="container">
    <small>Date: {{ current_date }}</small><br/>
    <small>File name: {{ document.fileName }}</small><br/> 
    <small>* {{ msg_amount_unit_date }}</small>    
      </div>
    </div>  
  </div>
  
<div class="container">  
    <h2>{{ title_personal_finance_score }}</h2>
    <span class="btn btn-{{ report.personal_finance_score_details.level }} btn-lg">{{ report.personal_finance_score_details.value|floatformat }}
        <span class="badge">
        {{ title_networth }} / {{ title_annual_spending }}</br>
        {{ report.networth|money|safe }} / {{ report.annual_spending|money|safe }}
        </span>
    </span>  
    <p>{{ report.personal_finance_score_details.message }}</p>
    
    <h2>{{ document|display:"f_CURRENTAMOUNT_INCOME" }} &amp; {{ document|display:"f_CURRENTAMOUNT_EXPENSE" }} *</h2>
    {% include "default/income_vs_expenditure_table.html" %}
    <div id="chart4" style="height: 300px; width: 600px"></div>
     
    <h2>{{ title_budget }}</h2>
    {% include "default/budget_table.html" %}
            
    <h2>{{ title_main_categories }}</h2>
    <table>
      <tr>
	<td align="center">
	  {% include "default/categories_previous_period_table.html" %}
	  <div id="chart1" style="height: 300px; width: 300px"></div>
	</td>
	<td align="center">
	  {% include "default/categories_period_table.html" %}
	  <div id="chart2" style="height: 300px; width: 300px"></div>
	</tr>
    </table>
    <h2>{{ title_variations }}</h2>
     {% include "default/categories_variations.html" %}
    
    <h2>{{ title_account }} *</h2>
     {% include "default/bank_table.html" %}
     <div id="chart5" style="height: 300px; width: 400px"></div>     
     <br/>
     {% include "default/account_table.html" %}
     
    <h2>{{ title_unit }} *</h2>
     {% include "default/unit_table.html" %}      
     
    <h2>{{ title_portfolio }} *</h2>
     {% include "default/portfolio.html" %}  
     {% if report.portfolio|length %}
     <div id="chart3" style="height: 300px; width: 400px"></div>
     {% endif %}
    <h2>{{ title_highlighted }}</h2>
    {% include "default/highlighted_operations.html" %}     
</div>

<script language="javascript">
    var item = document.getElementById("table_unit");
    if(item != null) item.className= "table table-bordered table-striped table-condensed";
    
    var item = document.getElementById("table_portfolio");
    if(item != null) item.className= "table table-bordered table-striped table-condensed";
    
    var item = document.getElementById("table_income_vs_expenditure");
    if(item != null) item.className= "table table-bordered table-striped table-condensed";
    
    var item = document.getElementById("table_categories_previous_period");
    if(item != null) item.className= "table table-bordered table-striped table-condensed";
    
    var item = document.getElementById("table_categories_period");
    if(item != null) item.className= "table table-bordered table-striped table-condensed";

    var item = document.getElementById("table_budget");
    if(item != null) item.className= "table table-bordered table-striped table-condensed";

    var item = document.getElementById("table_bank");
    if(item != null) item.className= "table table-bordered table-striped table-condensed";    
    
    var item = document.getElementById("table_account");
    if(item != null) item.className= "table table-bordered table-striped table-condensed";       
    
$(function () {
    $('#chart1').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: false,          
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                },
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                            window.open(this.options.url);
                        }
                    }
                }                
            }        
        },
        series: [{
            type: 'pie',
            name: '{{ title_main_categories }}',
            data: [
	    {% for item in report.categories_previous_period %}
		{% if forloop.first %}
		{% else %}    
			{name: '{{ forloop.counter0 }}', 
			y: {{ item.2 }}, 
			url: 'skg://Skrooge_operation_plugin/SKGOPERATION_CONSOLIDATED_DEFAULT_PARAMETERS/?operationTable=v_suboperation_consolidated&operationWhereClause={{ item.3|encode }}&title={{ item.1|encode }}/{{ report.categories_period.0.2 }}&title_icon=view-categories'}
			{% if forloop.last %}{% else %},{% endif %}
		{% endif %}
            {% endfor %}	    
            ]
        }]
    });
});

$(function () {
    $('#chart2').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: false,          
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }      
        },
        series: [{
            type: 'pie',
            name: '{{ title_main_categories }}',
            data: [
	    {% for item in report.categories_period %}
		{% if forloop.first %}
		{% else %}    
			{name: '{{ forloop.counter0 }}', 
			y: {{ item.2 }}, 
			url: 'skg://Skrooge_operation_plugin/SKGOPERATION_CONSOLIDATED_DEFAULT_PARAMETERS/?operationTable=v_suboperation_consolidated&operationWhereClause={{ item.3|encode }}&title={{ item.1|encode }}/{{ report.categories_period.0.2 }}&title_icon=view-categories'}
			{% if forloop.last %}{% else %},{% endif %}
		{% endif %}
            {% endfor %}	    
            ]
        }]
    });
});

$(function () {
    $('#chart3').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: false,          
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }      
        },
        series: [{
            type: 'pie',
            name: '',
            data: [
            {% for item in report.portfolio %}
	      {% if forloop.first %}
		{% else %}
		['{{ item.0 }}', {{ item.5 }}]
		{% if forloop.last %}{% else %},{% endif %}
	      {% endif %}
            {% endfor %}
            ]
        }]
    });
});



$(function () {
    $('#chart4').highcharts({
        chart: {
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 15,
                beta: 15,
                viewDistance: 25,
                depth: 40
            }
        },

        title: {
            text: ''
        },

        xAxis: {
            categories: ['{{ report.previous_period }}', '{{ report.period }}']
        },

        yAxis: {
            allowDecimals: true,
            min: 0,
            title: {
                text: ''
            }
        },
	colors: ['#{{ color_negativetext }}', '#{{ color_positivetext }}'],
	
        tooltip: {
            pointFormat: '<span style="color:{series.color}">\u25CF</span>{point.y}'
        },

        series: [{
            name: '{{ document|display:"f_CURRENTAMOUNT_EXPENSE" }}',
            data: [{{ report.income_vs_expenditure.2.2 }}, {{ report.income_vs_expenditure.2.3 }}0]
        }, {
            name: '{{ document|display:"f_CURRENTAMOUNT_INCOME" }}',
            data: [{{ report.income_vs_expenditure.1.2 }}, {{ report.income_vs_expenditure.1.3 }}]
        }
        ]
    });
});

$(function () {
    $('#chart5').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: false,
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },
        series: [{
            type: 'pie',
            name: '',
            data: [
            {% for item in report.bank_table %}
	      {% if forloop.first %}
		{% else %}
		{% if forloop.last %}
		  [null, null]
		{% else %}
		  ['{{ item.1 }}', {{ item.6 }}],
		{% endif %}
	      {% endif %}
            {% endfor %}
            ]
        }]
    });
});

</script> 
  </body>
</html>
