#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_IMPORTEXPORT ::..")

PROJECT(plugin_importexport)

FIND_PACKAGE(KF5 5.0.0 REQUIRED COMPONENTS 
  GuiAddons             # Tier 1
)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skrooge_importexport_SRCS
skgimportexportplugin.cpp)

ki18n_wrap_ui(skrooge_importexport_SRCS skgimportexportpluginwidget_pref.ui)
kconfig_add_kcfg_files(skrooge_importexport_SRCS skgimportexport_settings.kcfgc )

ADD_LIBRARY(skrooge_importexport MODULE ${skrooge_importexport_SRCS})
TARGET_LINK_LIBRARIES(skrooge_importexport KF5::Parts KF5::KIOCore KF5::KIOFileWidgets KF5::KIOWidgets KF5::KIONTLM skgbasemodeler skgbasegui skgbankmodeler)

########### install files ###############
INSTALL(TARGETS skrooge_importexport DESTINATION ${KDE_INSTALL_QTPLUGINDIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/org.kde.skrooge-plugin-importexport.desktop DESTINATION ${KDE_INSTALL_KSERVICES5DIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/skrooge_importexport.rc  DESTINATION  ${KDE_INSTALL_KXMLGUI5DIR}/skrooge_importexport )
INSTALL(FILES ${PROJECT_SOURCE_DIR}/skgimportexport_settings.kcfg  DESTINATION  ${KDE_INSTALL_KCFGDIR} )
