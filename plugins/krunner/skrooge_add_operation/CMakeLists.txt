#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_ADD_OPERATION ::..")

PROJECT(plugin_add_operation)

FIND_PACKAGE(KF5Runner)

IF(KF5Runner_FOUND)
  LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

  ADD_LIBRARY(skrooge_add_operation MODULE skgaddoperation.cpp)
  TARGET_LINK_LIBRARIES(skrooge_add_operation KF5::Runner KF5::WidgetsAddons KF5::I18n)

  ########### install files ###############
  INSTALL(TARGETS skrooge_add_operation DESTINATION ${KDE_INSTALL_QTPLUGINDIR})
  INSTALL(FILES ${PROJECT_SOURCE_DIR}/org.kde.plasma-runner-skrooge-add-operation.desktop DESTINATION ${KDE_INSTALL_KSERVICES5DIR})
ENDIF()
