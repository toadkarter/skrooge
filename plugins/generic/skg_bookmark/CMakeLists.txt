#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_BOOKMARK ::..")

PROJECT(plugin_bookmark)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skg_bookmark_SRCS skgbookmarkplugin.cpp skgbookmarkplugindockwidget.cpp)

ki18n_wrap_ui(skg_bookmark_SRCS skgbookmarkplugindockwidget_base.ui skgbookmarkpluginwidget_pref.ui)

kconfig_add_kcfg_files(skg_bookmark_SRCS skgbookmark_settings.kcfgc )

ADD_LIBRARY(skg_bookmark MODULE ${skg_bookmark_SRCS})
TARGET_LINK_LIBRARIES(skg_bookmark KF5::Parts KF5::ItemViews KF5::IconThemes skgbasemodeler skgbasegui )

########### install files ###############
INSTALL(TARGETS skg_bookmark DESTINATION ${KDE_INSTALL_QTPLUGINDIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/skgbookmark_settings.kcfg  DESTINATION  ${KDE_INSTALL_KCFGDIR} )
INSTALL(FILES ${PROJECT_SOURCE_DIR}/org.kde.skg-plugin-bookmark.desktop DESTINATION ${KDE_INSTALL_KSERVICES5DIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/skg_bookmark.rc  DESTINATION  ${KDE_INSTALL_KXMLGUI5DIR}/skg_bookmark )
