#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_DELETE ::..")

PROJECT(plugin_delete)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skg_delete_SRCS skgdeleteplugin.cpp)

ADD_LIBRARY(skg_delete MODULE ${skg_delete_SRCS})
TARGET_LINK_LIBRARIES(skg_delete KF5::Parts skgbasemodeler skgbasegui)

########### install files ###############
INSTALL(TARGETS skg_delete DESTINATION ${KDE_INSTALL_QTPLUGINDIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/org.kde.skg-plugin-delete.desktop DESTINATION ${KDE_INSTALL_KSERVICES5DIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/skg_delete.rc  DESTINATION  ${KDE_INSTALL_KXMLGUI5DIR}/skg_delete )
