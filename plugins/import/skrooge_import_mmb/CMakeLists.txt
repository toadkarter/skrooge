#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_IMPORT_MMB ::..")

PROJECT(plugin_import_mmb)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skrooge_import_mmb_SRCS
	skgimportpluginmmb.cpp
)

ADD_LIBRARY(skrooge_import_mmb MODULE ${skrooge_import_mmb_SRCS})
TARGET_LINK_LIBRARIES(skrooge_import_mmb KF5::Parts Qt5::Sql skgbasemodeler skgbasegui skgbankmodeler skgbankgui)

########### install files ###############
INSTALL(TARGETS skrooge_import_mmb DESTINATION ${KDE_INSTALL_QTPLUGINDIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/org.kde.skrooge-import-mmb.desktop DESTINATION ${KDE_INSTALL_KSERVICES5DIR})

