#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_IMPORT_LEDGER ::..")

PROJECT(plugin_import_ledger)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skrooge_import_ledger_SRCS
	skgimportpluginledger.cpp
)

ADD_LIBRARY(skrooge_import_ledger MODULE ${skrooge_import_ledger_SRCS})
TARGET_LINK_LIBRARIES(skrooge_import_ledger KF5::Parts Qt5::Core skgbasemodeler skgbasegui skgbankmodeler skgbankgui)

########### install files ###############
INSTALL(TARGETS skrooge_import_ledger DESTINATION ${KDE_INSTALL_QTPLUGINDIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/org.kde.skrooge-import-ledger.desktop DESTINATION ${KDE_INSTALL_KSERVICES5DIR})

