#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_IMPORT_XXX ::..")

PROJECT(plugin_import_xxx)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skrooge_import_xxx_SRCS
	skgimportpluginxxx.cpp
)

ADD_LIBRARY(skrooge_import_xxx MODULE ${skrooge_import_xxx_SRCS})
TARGET_LINK_LIBRARIES(skrooge_import_xxx KF5::Parts skgbasemodeler skgbasegui skgbankmodeler skgbankgui)

########### install files ###############
INSTALL(TARGETS skrooge_import_xxx DESTINATION ${KDE_INSTALL_QTPLUGINDIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/org.kde.skrooge-import-xxx.desktop DESTINATION ${KDE_INSTALL_KSERVICES5DIR})

